<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="rpg_maker_vx___carpets_autotiles_by_ayene_chan_d5og8bo" tilewidth="32" tileheight="32" tilecount="72" columns="12">
 <properties>
  <property name="tilesetCopyright" value="by Ayene - free to use if crediting Ayene"/>
 </properties>
 <image source="Tiles/rpg_maker_vx___carpets_autotiles_by_ayene_chan_d5og8bo.png" width="384" height="192"/>
</tileset>
