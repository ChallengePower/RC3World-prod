<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="floor2" tilewidth="32" tileheight="32" tilecount="2016" columns="32">
 <properties>
  <property name="tilesetCopyright">By telles0808 - Free for non-commercial use 
https://www.deviantart.com/telles0808/art/RPG-Maker-VX-RTP-Tileset-159218223</property>
 </properties>
 <image source="../tilesets/rpg_maker_vx_rtp_tileset_by_telles0808_d2mslkf.png" width="1024" height="2016"/>
 <wangsets>
  <wangset name="Terrains" type="corner" tile="-1">
   <wangcolor name="floor_tent" color="#ff0000" tile="1295" probability="1"/>
   <wangtile tileid="577" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="608" wangid="0,1,0,0,0,1,0,1"/>
   <wangtile tileid="609" wangid="0,1,0,1,0,0,0,1"/>
   <wangtile tileid="640" wangid="0,0,0,1,0,1,0,1"/>
   <wangtile tileid="641" wangid="0,1,0,1,0,1,0,0"/>
  </wangset>
 </wangsets>
</tileset>
