<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="courage_is_contagious" tilewidth="32" tileheight="32" tilecount="3" columns="3">
 <properties>
  <property name="tilesetCopyright" value="WTFPL - Whistleblow Village"/>
 </properties>
 <image source="Tiles/courage_is_contagious.png" width="96" height="32"/>
</tileset>
