<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="dbz1aoz-af48e951-df46-49da-873c-a85341406b30" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <properties>
  <property name="tilesetCopyright" value="Free to use for non-commercial - Enterbrain and Mack and milkian"/>
 </properties>
 <image source="Tiles/dbz1aoz-af48e951-df46-49da-873c-a85341406b30.png" width="512" height="512"/>
 <tile id="151">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="167">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
