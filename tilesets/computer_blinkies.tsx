<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="computer_blinkies" tilewidth="32" tileheight="32" tilecount="152" columns="19">
 <properties>
  <property name="tilesetCopyright">Copyright: Viktor Hahn
License: CC-BY 4.0 - blinkies remix WTFPL or CC-By Whistleblow Village + Viktor Hahn</property>
 </properties>
 <image source="computer_blinkies.png" width="608" height="256"/>
 <tile id="1">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="4" duration="292"/>
   <frame tileid="7" duration="222"/>
   <frame tileid="13" duration="402"/>
   <frame tileid="10" duration="492"/>
   <frame tileid="4" duration="372"/>
   <frame tileid="13" duration="153"/>
  </animation>
 </tile>
 <tile id="5">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="5" duration="734"/>
   <frame tileid="8" duration="234"/>
   <frame tileid="11" duration="543"/>
   <frame tileid="14" duration="234"/>
   <frame tileid="8" duration="292"/>
   <frame tileid="14" duration="453"/>
   <frame tileid="5" duration="342"/>
   <frame tileid="11" duration="123"/>
  </animation>
 </tile>
 <tile id="7">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="8" duration="501"/>
   <frame tileid="5" duration="321"/>
   <frame tileid="11" duration="123"/>
   <frame tileid="14" duration="231"/>
   <frame tileid="8" duration="191"/>
  </animation>
 </tile>
 <tile id="10">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="23" duration="153"/>
   <frame tileid="26" duration="353"/>
   <frame tileid="32" duration="283"/>
   <frame tileid="29" duration="153"/>
   <frame tileid="23" duration="433"/>
   <frame tileid="32" duration="493"/>
  </animation>
 </tile>
 <tile id="26">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="41" duration="342"/>
   <frame tileid="44" duration="1231"/>
   <frame tileid="50" duration="346"/>
   <frame tileid="47" duration="764"/>
   <frame tileid="44" duration="604"/>
  </animation>
 </tile>
 <tile id="42">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="42" duration="604"/>
   <frame tileid="45" duration="234"/>
   <frame tileid="51" duration="434"/>
   <frame tileid="48" duration="124"/>
   <frame tileid="51" duration="234"/>
   <frame tileid="45" duration="432"/>
  </animation>
 </tile>
 <tile id="43">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="43" duration="234"/>
   <frame tileid="49" duration="122"/>
   <frame tileid="46" duration="342"/>
   <frame tileid="43" duration="234"/>
   <frame tileid="52" duration="432"/>
   <frame tileid="49" duration="634"/>
   <frame tileid="46" duration="434"/>
  </animation>
 </tile>
 <tile id="44">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="45" duration="114"/>
   <frame tileid="42" duration="233"/>
   <frame tileid="51" duration="334"/>
   <frame tileid="48" duration="324"/>
   <frame tileid="45" duration="224"/>
  </animation>
 </tile>
 <tile id="46">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="43" duration="224"/>
   <frame tileid="52" duration="124"/>
   <frame tileid="49" duration="524"/>
   <frame tileid="52" duration="234"/>
   <frame tileid="46" duration="434"/>
   <frame tileid="49" duration="233"/>
  </animation>
 </tile>
 <tile id="47">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="60" duration="543"/>
   <frame tileid="63" duration="123"/>
   <frame tileid="66" duration="453"/>
   <frame tileid="69" duration="342"/>
  </animation>
 </tile>
 <tile id="63">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="76">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="79" duration="434"/>
   <frame tileid="82" duration="434"/>
   <frame tileid="85" duration="434"/>
   <frame tileid="88" duration="434"/>
  </animation>
 </tile>
 <tile id="80">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="80" duration="434"/>
   <frame tileid="83" duration="434"/>
   <frame tileid="86" duration="434"/>
   <frame tileid="89" duration="434"/>
   <frame tileid="83" duration="434"/>
  </animation>
 </tile>
 <tile id="82">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="79" duration="191"/>
   <frame tileid="85" duration="423"/>
   <frame tileid="88" duration="333"/>
   <frame tileid="82" duration="262"/>
   <frame tileid="85" duration="222"/>
   <frame tileid="88" duration="342"/>
  </animation>
 </tile>
 <tile id="83">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="96">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="99" duration="434"/>
   <frame tileid="102" duration="123"/>
   <frame tileid="105" duration="143"/>
   <frame tileid="108" duration="434"/>
   <frame tileid="105" duration="193"/>
  </animation>
 </tile>
 <tile id="102">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="114" duration="1000"/>
   <frame tileid="117" duration="1000"/>
  </animation>
 </tile>
 <tile id="115">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="115" duration="1000"/>
   <frame tileid="118" duration="1000"/>
  </animation>
 </tile>
 <tile id="116">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="116" duration="1000"/>
   <frame tileid="119" duration="1000"/>
  </animation>
 </tile>
 <tile id="117">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="117" duration="234"/>
   <frame tileid="120" duration="143"/>
   <frame tileid="126" duration="346"/>
   <frame tileid="123" duration="678"/>
   <frame tileid="120" duration="123"/>
   <frame tileid="117" duration="143"/>
  </animation>
 </tile>
 <tile id="118">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="118" duration="543"/>
   <frame tileid="124" duration="343"/>
   <frame tileid="121" duration="422"/>
   <frame tileid="127" duration="762"/>
   <frame tileid="124" duration="262"/>
  </animation>
 </tile>
 <tile id="119">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="119" duration="262"/>
   <frame tileid="125" duration="432"/>
   <frame tileid="122" duration="232"/>
   <frame tileid="128" duration="432"/>
   <frame tileid="122" duration="332"/>
   <frame tileid="128" duration="422"/>
  </animation>
 </tile>
 <tile id="120">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="117" duration="233"/>
   <frame tileid="123" duration="231"/>
   <frame tileid="120" duration="431"/>
   <frame tileid="123" duration="231"/>
   <frame tileid="126" duration="451"/>
  </animation>
 </tile>
 <tile id="121">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="121" duration="451"/>
   <frame tileid="127" duration="231"/>
   <frame tileid="124" duration="181"/>
   <frame tileid="118" duration="431"/>
   <frame tileid="121" duration="301"/>
   <frame tileid="118" duration="501"/>
  </animation>
 </tile>
 <tile id="122">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="125">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="126">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="127">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="128">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="133">
  <animation>
   <frame tileid="133" duration="1000"/>
   <frame tileid="136" duration="1000"/>
  </animation>
 </tile>
 <tile id="134">
  <animation>
   <frame tileid="134" duration="1000"/>
   <frame tileid="137" duration="1000"/>
  </animation>
 </tile>
 <tile id="135">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="135" duration="1000"/>
   <frame tileid="138" duration="1000"/>
  </animation>
 </tile>
 <tile id="138">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="138" duration="422"/>
   <frame tileid="141" duration="123"/>
   <frame tileid="144" duration="344"/>
   <frame tileid="138" duration="233"/>
   <frame tileid="141" duration="123"/>
   <frame tileid="147" duration="114"/>
  </animation>
 </tile>
 <tile id="141">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="147">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
