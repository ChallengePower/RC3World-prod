<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="Speech" tilewidth="32" tileheight="32" tilecount="64" columns="8">
 <properties>
  <property name="tilesetCopyright" value="based on CC0 https://opengameart.org/content/emotes-pack"/>
 </properties>
 <image source="Speech.png" width="256" height="256"/>
 <tile id="1">
  <animation>
   <frame tileid="23" duration="400"/>
   <frame tileid="1" duration="400"/>
   <frame tileid="2" duration="400"/>
   <frame tileid="3" duration="400"/>
   <frame tileid="6" duration="400"/>
  </animation>
 </tile>
 <tile id="9">
  <animation>
   <frame tileid="23" duration="400"/>
   <frame tileid="15" duration="400"/>
   <frame tileid="9" duration="400"/>
   <frame tileid="11" duration="400"/>
   <frame tileid="13" duration="400"/>
  </animation>
 </tile>
 <tile id="10">
  <animation>
   <frame tileid="23" duration="350"/>
   <frame tileid="15" duration="350"/>
   <frame tileid="9" duration="350"/>
   <frame tileid="11" duration="350"/>
   <frame tileid="13" duration="350"/>
   <frame tileid="15" duration="400"/>
  </animation>
 </tile>
 <tile id="17">
  <animation>
   <frame tileid="23" duration="400"/>
   <frame tileid="15" duration="400"/>
   <frame tileid="19" duration="400"/>
   <frame tileid="20" duration="400"/>
   <frame tileid="19" duration="400"/>
   <frame tileid="20" duration="400"/>
   <frame tileid="21" duration="400"/>
   <frame tileid="20" duration="400"/>
  </animation>
 </tile>
 <tile id="23">
  <animation>
   <frame tileid="23" duration="400"/>
   <frame tileid="15" duration="400"/>
   <frame tileid="9" duration="400"/>
   <frame tileid="11" duration="400"/>
   <frame tileid="14" duration="400"/>
  </animation>
 </tile>
 <tile id="25">
  <animation>
   <frame tileid="23" duration="400"/>
   <frame tileid="15" duration="400"/>
   <frame tileid="29" duration="400"/>
   <frame tileid="30" duration="400"/>
   <frame tileid="28" duration="400"/>
   <frame tileid="30" duration="400"/>
   <frame tileid="29" duration="400"/>
   <frame tileid="30" duration="400"/>
   <frame tileid="28" duration="400"/>
   <frame tileid="30" duration="400"/>
   <frame tileid="29" duration="400"/>
  </animation>
 </tile>
 <tile id="32">
  <animation>
   <frame tileid="32" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="19" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="40" duration="4000"/>
   <frame tileid="41" duration="4000"/>
   <frame tileid="33" duration="4000"/>
  </animation>
 </tile>
 <tile id="34">
  <animation>
   <frame tileid="40" duration="4000"/>
   <frame tileid="42" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="19" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="44" duration="4000"/>
   <frame tileid="46" duration="4000"/>
  </animation>
 </tile>
 <tile id="36">
  <animation>
   <frame tileid="41" duration="4000"/>
   <frame tileid="43" duration="4000"/>
   <frame tileid="45" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="19" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="47" duration="4000"/>
  </animation>
 </tile>
 <tile id="38">
  <animation>
   <frame tileid="33" duration="4000"/>
   <frame tileid="35" duration="4000"/>
   <frame tileid="37" duration="4000"/>
   <frame tileid="39" duration="1000"/>
   <frame tileid="20" duration="1000"/>
   <frame tileid="19" duration="1000"/>
   <frame tileid="20" duration="1000"/>
  </animation>
 </tile>
</tileset>
