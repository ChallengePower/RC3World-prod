<tileset version="1.5" tiledversion="1.7.2" name="monkey_island" tilewidth="32" tileheight="32" tilecount="1014" columns="78" objectalignment="top">
 <image source="monkey_island_scale.png" width="2500" height="435" />
 <tile id="475">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="476">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="477">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="546">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="547">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="548">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="549">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="550">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="551">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="552">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="553">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="554">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="555">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="616">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="617">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="618">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="619">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="620">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="621">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="622">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="623">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="624">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="625">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="630">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="631">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="632">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="633">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="634">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="635">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="636">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="637">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="638">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="639">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="640">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="641">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="642">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="643">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="644">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="645">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="646">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="647">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="694">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="695">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="696">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="697">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="698">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="699">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="700">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="701">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="702">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="703">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="707">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="708">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="709">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="710">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="711">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="712">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="713">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="714">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="715">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="716">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="717">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="718">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="719">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="720">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="721">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="722">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="723">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="724">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="725">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="726">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="727">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="728">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="729">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="730">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="731">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="732">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="733">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="734">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="735">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="736">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="737">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="738">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="739">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="740">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="741">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="742">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="743">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="744">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="745">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="746">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="747">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="748">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="749">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="750">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="751">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="752">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="753">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="754">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="755">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="756">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="757">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="758">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="759">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="760">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="761">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="762">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="763">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="764">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="765">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="766">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="767">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="768">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="769">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="770">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="771">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="772">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="773">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="774">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="775">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="776">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="777">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="778">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="779">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="780">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="781">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="787">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="789">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="790">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="791">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="792">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="793">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="794">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="795">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="796">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="797">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="798">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="799">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="800">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="801">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="802">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="803">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="804">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="805">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="806">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="807">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="808">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="809">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="810">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="811">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="812">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="813">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="814">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="815">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="816">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="817">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="818">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="819">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="820">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="821">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="822">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="823">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="824">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="825">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="826">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="827">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="828">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="829">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="830">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="831">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="832">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="833">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="834">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="835">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="836">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="837">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="838">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="839">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="840">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="841">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="842">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="843">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="844">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="845">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="846">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="847">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="848">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="849">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="850">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="858">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="859">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="883">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="884">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="885">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="886">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="887">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="888">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="889">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="890">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="891">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="892">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="893">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="894">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="895">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="896">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="897">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="898">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="899">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="900">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="901">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="902">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="903">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="904">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="905">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="906">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="907">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="908">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="909">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="910">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="911">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="912">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="913">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="914">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="915">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="916">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="917">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="936">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="937">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="938">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="939">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="940">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="941">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1005">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1006">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1007">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1008">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1009">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1010">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1011">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1012">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1013">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
<properties><property name="tilesetCopyright" value="free for non-commercial use" /></properties></tileset>