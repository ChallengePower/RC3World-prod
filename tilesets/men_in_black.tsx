<tileset version="1.5" tiledversion="1.8.0" name="men_in_black" tilewidth="32" tileheight="32" tilecount="32" columns="4">
 <image source="men_in_black.png" width="128" height="256" />
 <tile id="0">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="4" duration="3456" />
   <frame tileid="8" duration="600" />
   <frame tileid="4" duration="2987" />
   <frame tileid="8" duration="890" />
  </animation>
 </tile>
 <tile id="5">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="20" duration="6880" />
   <frame tileid="16" duration="480" />
   <frame tileid="20" duration="4690" />
   <frame tileid="16" duration="320" />
  </animation>
 </tile>
 <tile id="21">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="21" duration="223" />
   <frame tileid="17" duration="223" />
  </animation>
 </tile>
 <tile id="22">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="22" duration="7420" />
   <frame tileid="18" duration="420" />
   <frame tileid="22" duration="4420" />
   <frame tileid="18" duration="335" />
  </animation>
 </tile>
 <tile id="23">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="25" duration="1420" />
   <frame tileid="17" duration="2270" />
   <frame tileid="25" duration="1270" />
   <frame tileid="17" duration="970" />
   <frame tileid="21" duration="270" />
   <frame tileid="17" duration="370" />
  </animation>
 </tile>
 <tile id="26">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="27" duration="3220" />
   <frame tileid="19" duration="890" />
   <frame tileid="27" duration="5420" />
   <frame tileid="19" duration="320" />
  </animation>
 </tile>
 <tile id="28">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="collides" type="bool" value="true" />
  </properties>
  <animation>
   <frame tileid="29" duration="5987" />
   <frame tileid="25" duration="5987" />
  </animation>
 </tile>
<properties><property name="tilesetCopyright" value="WTFPL - Whistleblow Village" /></properties></tileset>