<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.8.0" name="urban_streets_c_by_schwarzenacht_d8cmhcy" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <properties>
  <property name="mapCopyright">by PandaMaru - free for non-commercial works
https://www.deviantart.com/schwarzenacht/art/urban-streets-C-504933730</property>
  <property name="tilesetCopyright" value="free for non-commercial use"/>
 </properties>
 <image source="urban_streets_c_by_schwarzenacht_d8cmhcy.png" width="512" height="512"/>
 <tile id="53">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="133">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="149">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="168">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="173">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="189">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="240">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
